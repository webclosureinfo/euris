import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { NgModule } from '@angular/core';
import { ConfirmationService, MessageService } from 'primeng/api';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppLayoutModule } from './layout/app.layout.module';
import { ExcerptFilter } from './layout/pipe/ExcerptFilter';
@NgModule({
    declarations: [
        AppComponent,
        ExcerptFilter
    ],
    imports: [
        AppRoutingModule,
        AppLayoutModule,
    ],
    providers: [
        ConfirmationService,
        { provide: LocationStrategy, useClass: HashLocationStrategy },
        MessageService, ConfirmationService,
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
