import { Component, OnInit } from '@angular/core';
import { LayoutService } from './service/app.layout.service';

@Component({
    selector: 'app-menu',
    templateUrl: './app.menu.component.html'
})
export class AppMenuComponent implements OnInit {

    model: any[] = [];

    constructor(public layoutService: LayoutService) { }

    ngOnInit() {
        this.model = [
            {
                label: 'Anagrafica',
                items: [
                    { label: 'Elenco', icon: 'pi pi-fw pi-home', routerLink: ['/customers'] },
                    { label: 'Nuovo', icon: 'pi pi-fw pi-id-card', routerLink: ['/customer'] },
                ]
            },
            {
                label: 'Categorie',
                items: [
                    { label: 'Elenco', icon: 'pi pi-fw pi-home', routerLink: ['/categories'] },
                    { label: 'Nuovo', icon: 'pi pi-fw pi-id-card', routerLink: ['/category'] },
                ]
            },
            {
                label: 'Autovalutazione',
                items: [
                    { label: 'Elenco', icon: 'pi pi-fw pi-home', routerLink: ['/forms'] },
                    { label: 'Nuovo', icon: 'pi pi-fw pi-id-card', routerLink: ['/form'] },
                ]
            }
        ];
    }
}
