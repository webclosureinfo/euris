import { Component } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { Product } from '../../models/product';
import { PaginatorRecap } from '../../models/table/paginator';
import { DashboardService } from '../../service/dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {
  products: Product[] = []
  polarData: any = []
  polarOptions: any = {}
  loading = true
  currentActiveIndex = -1
  expand = false
  review = ''

  paginator: PaginatorRecap = {
    page: 1,
    size: 5,
    count: 0
  }

  constructor(private service: DashboardService, private confirmationService: ConfirmationService) {
    this.search()

  }

  setPaginatorRecap(event: any) {
    this.paginator.page = event.page + 1
    this.paginator.size = event.rows
    this.search()
  }

  search() {
    this.service.getProducts(this.paginator.page, this.paginator.size).subscribe(responses => {
      this.products = responses.list;
      this.loading = false
      this.paginator.count = responses.length
      if (!this.polarData.datasets.length) {
        this.polarData = this.getChartData(responses.length)
      }
    });
  }


  groupBy(objectArray, property) {
    return objectArray.reduce(function (acc, obj) {
      var key = obj[property];
      if (!acc[key]) {
        acc[key] = [];
      }
      acc[key].push(obj);
      return acc;
    }, {});
  }
  getChartData(size: number) {
    this.service.getProducts(1, size).subscribe(responses => {
      this.setChart(responses.list)
    });
  }

  setChart(input: Product[]) {
    let j = 0;
    const data = []
    const labels = []
    const datas = input.map((e) => e.data)
    const f = this.groupBy(datas, 'category')

    for (let key in f) {
      labels.push(key)
      let c = 0
      for (let i of f[key]) {
        c += parseFloat(i.price)
      }
      data.push(c)
    }

    const documentStyle = getComputedStyle(document.documentElement);
    const textColor = documentStyle.getPropertyValue('--text-color');
    const surfaceBorder = documentStyle.getPropertyValue('--surface-border');

    this.polarData = {
      datasets: [{
        data: data,
        backgroundColor: [
          documentStyle.getPropertyValue('--indigo-500'),
          documentStyle.getPropertyValue('--purple-500'),
          documentStyle.getPropertyValue('--teal-500'),
          documentStyle.getPropertyValue('--orange-500'),
          documentStyle.getPropertyValue('--pink-500'),
          documentStyle.getPropertyValue('--red-500'),
          documentStyle.getPropertyValue('--yellow-500'),
          documentStyle.getPropertyValue('--green-500'),
          documentStyle.getPropertyValue('--gray-500'),
        ],
        label: 'Reparti'
      }],
      labels: labels
    };

    this.polarOptions = {
      plugins: {
        legend: {
          labels: {
            color: textColor
          }
        }
      },
      scales: {
        r: {
          grid: {
            color: surfaceBorder
          }
        }
      }
    };
  }

  setActive(index: number) {
    this.currentActiveIndex = index
  }

  deleteReview(id: string, index: number) {
    this.confirmationService.confirm({
      target: event.target as EventTarget,
      message: 'Confermi di voler eliminare la recensione?',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.service.deleteReview(id).subscribe((response) => {
          this.products[this.currentActiveIndex].data.reviews.splice(index, 1)
        })
      },
      reject: () => {
      }
    });
  }
  toggleExpand() {
    this.expand = !this.expand
  }
  addReview() { }
}
