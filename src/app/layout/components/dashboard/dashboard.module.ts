import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DashboardsRoutingModule } from './dashboard-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    DashboardsRoutingModule,
  ]
})
export class DashboardModule { }
