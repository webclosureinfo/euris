import { Component, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem } from 'primeng/api';
import { Store } from './models/store';
import { LayoutService } from "./service/app.layout.service";
import { DashboardService } from './service/dashboard.service';

@Component({
    selector: 'app-topbar',
    templateUrl: './app.topbar.component.html'
})
export class AppTopBarComponent {
    store: Store = {
        name: '',
        employees: [],
        category: ''
    };

    items!: MenuItem[];

    menuItems: MenuItem[] = [];

    @ViewChild('menubutton') menuButton!: ElementRef;

    @ViewChild('topbarmenubutton') topbarMenuButton!: ElementRef;

    @ViewChild('topbarmenu') menu!: ElementRef;

    constructor(public layoutService: LayoutService, public router: Router, private service: DashboardService) {
        this.service.getStore().subscribe((response) => {
            this.store = response
        })
        this.menuItems = [
            {
                label: 'Logout', icon: 'pi pi-sign-out', command: () => {
                    console.log("LOGOUT");
                    this.router.navigate(['/auth/login'])
                }
            },
        ];
    }
}
