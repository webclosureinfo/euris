import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'excerpt'
})
export class ExcerptFilter implements PipeTransform {
    transform(text: string, length: number): string {
        if (text.length > length) {
            return text.substr(0, length) + '...';
        }
        return text;
    }
}