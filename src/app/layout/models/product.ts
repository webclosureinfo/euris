import { ProductData } from "./productData";

export interface Product {
    id: string;
    data: ProductData;
}
