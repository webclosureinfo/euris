export interface PaginatorRecap {
    page: number;
    size: number;
    count: number;
}