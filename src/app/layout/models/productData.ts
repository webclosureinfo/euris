export interface ProductData {
    reviews: string[];
    price: number;
    description: string;
    title: string;
    category: string;
    employee: string;
}