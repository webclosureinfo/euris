import { Product } from "../../product";

export interface ProductRequest {
    list: Product[];
    length: number;
}