import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { baseUrl, storeId } from '../config/constant';
import { ProductRequest } from '../models/http/request/productRequest';
import { Store } from '../models/store';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http: HttpClient) { }

  public getStore(): Observable<Store> {
    return this.http.get<Store>(`${baseUrl}/stores/${storeId}`)
  }
  public getProducts(page: number, size: number): Observable<ProductRequest> {
    return this.http.get<ProductRequest>(`${baseUrl}/stores/${storeId}/products?page=${page}&elements=${size}`)
  }

  public deleteReview(id) {
    return this.http.delete<ProductRequest>(`${baseUrl}/stores/${storeId}/products/${id}`)
  }
}
